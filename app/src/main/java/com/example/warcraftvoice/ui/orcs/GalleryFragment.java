package com.example.warcraftvoice.ui.orcs;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.warcraftvoice.R;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private  MediaPlayer mp = null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                new ViewModelProvider( this ).get( GalleryViewModel.class );
        View root = inflater.inflate( R.layout.fragment_orcs, container, false );
        final ImageView hp = root.findViewById( R.id.hp );
        hp.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                playThatSound();
            }
        } );
        return root;
    }

    public void playThatSound(){
        if (mp != null) {
            mp.reset();
            mp.release();
        }

      //  mp = MediaPlayer.create(this, R.raw.jenepeuxrien);
        mp.start();
    }
}