package com.example.warcraftvoice.ui.site;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.warcraftvoice.R;

public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                new ViewModelProvider( this ).get( SlideshowViewModel.class );
        View root = inflater.inflate( R.layout.fragment_site, container, false );
        final WebView m = root.findViewById(R.id.wikipedia);
        m.getSettings().setLoadWithOverviewMode(true);
        m.getSettings().setUseWideViewPort(true);
        WebSettings webSettings = m.getSettings();
        webSettings.setJavaScriptEnabled(true);
        m.loadUrl("https://fr.wikipedia.org/wiki/Warcraft");
        m.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        return root;
    }
}